require('../config/db');
var UserModel = require('../model/user.model');
const jwt = require('jsonwebtoken');
const jwtConf = require('./../config/jwtSecKey');
const hashPassword = require('./../hashing/hashPassword');
const verifyPassword = require('./../hashing/verifyPassword');

function fetchUser(req, res, next) {

    UserModel.find({})
        .sort({
            _id: -1
        })
        .exec(function (err, user) {
            if (err) {
                return next(err)
            }
            res.json(user);
        })
}

function registerUser(req, res, next) {

    var newUser = new UserModel({});
    const { firstname, lastname, username, password, address, email, gender, phoneNumber, role } = req.body
    hashPassword(password)
        .then(hash => {
            console.log('after hashed>>', hash);

            newUser.firstName = firstname;
            newUser.lastName = lastname;
            newUser.address = address;
            newUser.email = email;
            newUser.username = username;
            newUser.password = hash;
            newUser.gender = gender;
            newUser.phoneNumber = phoneNumber;
            newUser.role = role;

            newUser.save()
                .then(function (data) {
                    res.json({
                        msg: 'sucess creating & registered user',
                        data
                    });
                })
                .catch(function (err) {
                    res.json(err);
                })
        })
        .catch(next);

}

function loginUser(req, res, next) {

    let { username, password } = req.body;
    UserModel.findOne({
        username: username
    })
        .exec(function (err, user) {
            if (err) {
                console.log('asdjajf');
                return next(err);
            }
            if (user) {
                console.log('in login');
                verifyPassword(password, user.password)
                    .then(function (isMatched) {
                        if (isMatched) {
                            //generate token
                            //id: user._id
                            const token = jwt.sign({ username }, jwtConf.jwtSecret,{expiresIn:"1d"});
                            res.status(200)
                                .json({ msg:'login sucessful', token });
                        }
                        else {
                            res.status(401).json({ message: ' login crediential invalid || password incorrect' })
                        }
                    })

            }
            else {
                res.status(401).json({ message: ' login crediential invalid' })
            }
        })
}

module.exports = {
    fetchUser,
    registerUser,
    loginUser
}

