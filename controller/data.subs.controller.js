require('./../config/db');
var dataModel = require('./../model/data.model');

var moment = require('moment');
var _ = require('lodash');

function fetchData(req, res, next) {

    if (req.query.from && req.query.to) {
        dataModel.find({ createdAt: { $gte: new Date(req.query.from), $lte: new Date(req.query.to) } })
            .then(function (data) {
                return res.json(inFormated(data));
            })
            .catch(function (err) {
                return next(err);
            })

        // dataModel.find()
        //     .then(function (data) {
        //         return res.json(inFormated(data));
        //     })
        //     .catch(function (err) {
        //         return next(err);
        //     })

    }
    if (req.query.from) {
        dataModel.find({ createdAt: { $gte: new Date(req.query.from) } })
            .then(function (data) {
                return res.json(inFormated(data));
            })
            .catch(function (err) {
                return next(err);
            })
    }

}

function inFormated(data) {
    let groupedTopic = _.groupBy(data, function (item) {
        return item.topic;
    })
    let keysTopic = (Object.keys(groupedTopic));
    let valuesTopic = (Object.values(groupedTopic));
    let grpData = [];

    keysTopic.forEach(function (itemK, indexK) {
        let tempMsg = [];
        let tempGrp = {};
        valuesTopic[indexK].forEach(function (itemV, indexV) {
            tempdata = {};
            tempdata.time = itemV.createdAt;
            tempdata.msg = itemV.msg;
            tempMsg.push(tempdata);
        })
        tempGrp.topic = itemK;
        tempGrp.message = tempMsg;
        grpData.push(tempGrp);
    })

    return grpData;
}

module.exports = {
    fetchData
}

// let groupedDate = _.groupBy(data, function (item) {
//     return moment.utc(item.createdAt).format("YYYY-MM-DD");
// })

// let keysDate = (Object.keys(groupedDate));
// let valuesDate = (Object.values(groupedDate));
//console.log('c>', valuesDate.length);

// let formattedValuesDate = [];
// valuesDate.forEach(function (i, ind) {
//     let groupedTopic = _.groupBy(i, function (item) {
//         return item.topic;
//     })
//     let keysTopic = (Object.keys(groupedTopic));
//     let valuesTopic = (Object.values(groupedTopic));
//     let grpData = [];

//     keysTopic.forEach(function (itemK, indexK) {
//         let tempMsg = [];
//         let tempGrp = {};
//         valuesTopic[indexK].forEach(function (itemV, indexV) {
//             tempdata = {};
//             tempdata.time = itemV.createdAt;
//             tempdata.msg = itemV.msg;
//             tempMsg.push(tempdata);
//         })
//         tempGrp.topic = itemK;
//         tempGrp.message = tempMsg;
//         grpData.push(tempGrp);
//     })

//         ;
//     formattedValuesDate.push(grpData);
// })

// let formattedData = {};
// keysDate.forEach(function (item, index) {
//     formattedData[item] = formattedValuesDate[index];
// })
