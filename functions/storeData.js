require('./../config/db');
var dataModel = require('./../model/data.model');

const emitter = require('../eventMediator');

function saveData(topic, message) {

    var newData = new dataModel({});

    newData.topic = topic;
    newData.msg = message;
    newData.timestamp = new Date().toISOString();

    newData.save()
        .then(function (data) {
            //console.log('saved>>')
        })
        .catch(function (err) {
            console.log('error saving>> ', err);
        })
}

emitter.on('mqtt-message', ({ topic, message }) => {
    saveData(topic, message);
    console.log('saved to DB');
})


