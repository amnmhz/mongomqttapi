//socket.io
// const http = require('http').createServer();

var express = require('express');
var app = express();
var cors = require('cors')
port = process.env.PORT || 9090; // PORT=8000 node server.js
require('./config/db');

//event emitter
const emitter = require('./eventMediator');

const server = app.listen(port, function (err, done) {
    if (err) {
        console.log('failed');
        return;
    }
    console.log('server listening at port', port);
})

//const io = require('socket.io')(server);
//mqtt
require('./mqttConfig');
require('./functions/storeData');

require('./socketConfig')(server);


//load router
var dataRouter = require('./router/data.route');
var authRouter = require('./router/auth.user.route');

//middlewares
var authentication = require('././middleware/authentication');

app.use(express.urlencoded({
    extended: false
}))

app.use(express.json())


app.use(cors());
app.use('/auth', authRouter);
app.use(authentication, dataRouter);


app.use(function (req, res, next) {
    next({
        msg: 'error 404 not found',
        status: 404
    })
})

///error handling middleware
app.use(function (err, req, res, next) {
    console.log('middleware>>', err);
    res.status(err.status || 400);
    res.json({
        message: err.msg || err
    })
})
