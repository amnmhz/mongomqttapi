const bcrypt = require('bcrypt');
const BPromise = require('bluebird');


const passwordVerification = (password, userHash) => {
    return new BPromise((resolve, reject) => {
        console.log("input pass>>", password);
        console.log('in user hashed>>', userHash);

        bcrypt.compare(password, userHash, function (err, ismatched) {
            if (err) {
                return reject(false);
            }
            return resolve(ismatched);
        })
    })
}

module.exports = passwordVerification;
