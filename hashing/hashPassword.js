const BPromise = require('bluebird');
const bcrypt = require('bcrypt');

const hashPassword = (password) => {
    console.log('test passwordhash >> inputpassword>>>', password);
    return new BPromise((resolve, reject) => {
        if (!password) {
            return reject(false);
        }
        const saltrounds = 10;
        bcrypt.genSalt(saltrounds, function (err, salt) {
            if (err) {
                return reject(err);
            }
            bcrypt.hash(password, salt, function (hasherr, hashed) {
                if (hasherr) {
                    return reject(hasherr);
                }
                console.log('hashed >>',hashed)
                return resolve(hashed);
            })
        })
    })
}

module.exports = hashPassword;

// function hashPassword(password){
//     console.log('test passwordhash >> inputpassword>>>', password);

//         const saltrounds = 10;
//         bcrypt.genSalt(saltrounds, function (err, salt) {
//             if (err) {
//                 return (err);
//             }
//             bcrypt.hash(password, salt, function (hasherr, hashed) {
//                 if (hasherr) {
//                     return (hasherr);
//                 }
//                 console.log('hashed >>',hashed)
//                 return (hashed);
//             })
//         })
// }