var express = require('express');
var router = express.Router();
var dataController = require('./../controller/data.subs.controller');


router.route('/fetchData')
    .get(dataController.fetchData);

module.exports = router;