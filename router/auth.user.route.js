var express = require('express');
var router = express.Router();

var authController = require('./../controller/auth.user.controller');

router.route('/register')
    .post(authController.registerUser);

router.route('/login')
    .post(authController.loginUser);

router.route('/users')
    .get(authController.fetchUser);

module.exports = router;