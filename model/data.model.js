var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dataSchema = new Schema({
    // topic: {
    //     type: String,
    //     unique: true
    // },
    topic: String,
    msg: Object,
}, {
    timestamps: true
}
);

module.exports = mongoose.model('hello1', dataSchema);
