module.exports = (server) => {
    const emitter = require('./eventMediator');

    const io = require('socket.io')(server);
    io.on("connection", (socket) => {
        emitter.on('mqtt-message', ({ topic, message }) => {
            console.log(' socket emitted >>>',message);
            socket.emit('mqtt-message', message);
        })
    })
}


