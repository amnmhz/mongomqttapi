var jwt = require('jsonwebtoken');
var jwtConfig = require('./../config/jwtSecKey');


module.exports = function(req,res,next){

    var token
    if (req.headers['x-access-token']){
        token = req.headers['x-access-token']; 
    }
    if (req.headers['authorization']){
        token = req.headers['authorization'];
    }
    if (req.headers['token']){
        token = req.headers['token'];
    }
    // if (req.query.token){
    //     token = req.query.token;
    // }
    // if (req.query['authorization']){
    //     token = req.query['authorization'];
    // }
    // if (req.query['x-access-token']){
    //     token = req.query['x-access-token'];
    // }

    if (token){
        jwt.verify(token,jwtConfig.jwtSecret,function(err,done){
            if (err){
                //return next({msg: "you are not authorized"});
                res.status(401).send({message:'you are not authorized || invalid token'})
            }
            return next();
        })
    }
    else(
        res.status(401).send({ message: 'token not provided' })
        // next(
        //     {msg : 'token not provided'}
        // )
    )
}